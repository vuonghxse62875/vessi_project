/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demo;

import login.CalculationLogic;

/**
 *
 * @author Vessi
 */
public class Calculator {
    public static void main(String[] args) {
        int a, b;
        a = 5; b = 2;
        
        System.out.println(a + " + " + b + " = " + CalculationLogic.add(a, b));
        System.out.println(a + " - " + b + " = " + CalculationLogic.sub(a, b));
        System.out.println(a + " * " + b + " = " + CalculationLogic.mul(a, b));
    }
}
